import * as plugins from './cloudflare.plugins.js';

export class CloudflareZone {
  public static createFromApiObject(apiObject: plugins.ICloudflareTypes['Zone']) {
    const cloudflareZone = new CloudflareZone();
    Object.assign(cloudflareZone, apiObject);
    return cloudflareZone;
  }
}
