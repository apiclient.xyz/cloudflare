import * as smartlog from '@push.rocks/smartlog';
import * as smartpromise from '@push.rocks/smartpromise';
import * as smartdelay from '@push.rocks/smartdelay';
import * as smartrequest from '@push.rocks/smartrequest';
import * as smartstring from '@push.rocks/smartstring';
import * as tsclass from '@tsclass/tsclass';

export { smartlog, smartpromise, smartdelay, smartrequest, smartstring, tsclass };

// third party
import * as cloudflare from 'cloudflare';
import type { Zone } from 'cloudflare/resources/zones/zones.js';
import type { Record } from 'cloudflare/resources/dns/records.js';

export interface ICloudflareTypes {
  Zone: Zone;
  Record: Record;
}

export { cloudflare };
